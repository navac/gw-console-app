﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW_STS_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create a character for the player
            Character player = new Character("C", 20, 20, 0);
            SkillLoadout pSkills = player.GetSkills();
            pSkills.AddSkill(new Fireball());           // 0
            pSkills.AddSkill(new FireAttunement());     // 1
            pSkills.AddSkill(new ReversalOfFortune());  // 2
            pSkills.AddSkill(new LightningJavelin());   // 3

            // Create a monster to fight
            Character monster = new Character("M", 20, 20, 1);

            List<Character> allCharacters = new List<Character>();
            allCharacters.Add(player);
            allCharacters.Add(monster);

            #region Skill and Buff Tests 1
            /*
            Console.WriteLine("HP = " + player.GetHealth());

            // Test 1
            // Cast fireball on self. Should do 10 damage
            pSkills.Cast(0, player);
            player.OnTurn();

            // Test 2
            // Cast Fire Attunement, then cast Fireball on self.
            // Should do 15 damage
            pSkills.Cast(1, player);
            pSkills.Cast(0, player);
            player.OnTurn();

            // Test 2.5
            // Cast Fire Attunement, then cast Lightning Javelin on self.
            // Should do 10 damage
            pSkills.Cast(1, player);
            pSkills.Cast(3, player);
            player.OnTurn();

            // Test 3
            // Cast Reversal of Fortune, then Fireball on self.
            // Reversal of Fortune should heal 10.
            // Fireball should do 0.
            pSkills.Cast(2, player);
            pSkills.Cast(0, player);
            player.OnTurn();

            // Test 4
            // Cast Reversal of Fortune
            // Cast Fire Attunement, then Fireball on self.
            // Reversal of Fortune should heal 15.
            // Fireball should do 0.
            pSkills.Cast(2, player);
            pSkills.Cast(1, player);
            pSkills.Cast(0, player);
            player.OnTurn();

            */
            #endregion

            // Create a test 6x5 map
            Map map = new Map(5, 6);
            map.PlaceCharacter(player, 0, 0);
            map.PlaceCharacter(monster, 4, 5);
            player.Print();

            #region Map Test 

            // Insert the player into 0,0 and re-draw
            /*
            map.PlaceCharacter(player, 0, 0);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Down);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Right);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Up);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Up);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Right);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Right);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Down);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Down);
            map.Print();

            map.MoveCharacter(player, Map.Direction.Left);
            map.Print();
            */

            #endregion

            while (true)
            {
                // Every loop in this is considered a turn

                // Print the map first
                map.Print();

                // Get the players actions for the turn
                HandleInputActionChoice(map, player);

                // Fire off all characters turn listerners
                allCharacters.ForEach((Character c) => { c.OnTurn(); });

                // Filter out the dead characters from the map
                List<Character> deadCharacters = allCharacters.FindAll((Character c) =>
                {
                    return c.IsDead();
                });
                allCharacters.RemoveAll((Character c) =>
                {
                    return c.IsDead();
                });

                // Fire off death triggers
                deadCharacters.ForEach((Character c) =>
                {
                    if (c.IsDead())
                    {
                        map.ClearCharacter(c);
                        c.OnDeath();
                    }
                });
            }
        }

        // TODO: Make a Game Logic class
        // TODO: Make an input handling class that genericizes options
        // TODO: simplify this logic
        private static void HandleInputActionChoice(Map map, Character player)
        {
            while (true)
            {
                Console.WriteLine("[0]: Move");
                Console.WriteLine("[1]: Skills");
                ConsoleKeyInfo input = Console.ReadKey(true);
                switch (input.Key)
                {
                    case ConsoleKey.NumPad0:
                        Map.Direction dir = HandleInputMove();
                        map.MoveCharacter(player, dir);
                        return;
                    case ConsoleKey.NumPad1:
                        InputSkillPayload skillInfo = HandleInputSkill(map, player);
                        player.GetSkills().Cast(skillInfo.skillIndex, skillInfo.target);
                        return;
                    default:
                        Console.WriteLine("Invalid command; try again.");
                        break;
                }
            }
        }

        private static Map.Direction HandleInputMove()
        {
            while (true)
            {
                Console.WriteLine("Move: ");
                Console.WriteLine("\t Up [0]");
                Console.WriteLine("\t Down [1]");
                Console.WriteLine("\t Left [2]");
                Console.WriteLine("\t Right [3]");

                ConsoleKeyInfo moveInput = Console.ReadKey(true);
                switch (moveInput.Key)
                {
                    case ConsoleKey.NumPad0:
                        return Map.Direction.Up;
                    case ConsoleKey.NumPad1:
                        return Map.Direction.Down;
                    case ConsoleKey.NumPad2:
                        return Map.Direction.Left;
                    case ConsoleKey.NumPad3:
                        return Map.Direction.Right;
                    default:
                        Console.WriteLine("Invalid command; try again.");
                        continue;
                }
            }
        }

        private struct InputSkillPayload
        {
            public int skillIndex;
            public Character target;
        }

        private static InputSkillPayload HandleInputSkill(Map map, Character c)
        {
            int skillIndex = -1;
            Character target = null;
            while (skillIndex == -1 || target == null)
            {
                SkillLoadout skills = c.GetSkills();
                skills.Print();

                // Try getting a skill they want to cast first
                ConsoleKeyInfo skillInput = Console.ReadKey(true);
                switch (skillInput.Key)
                {
                    // TODO: Ensure inputs map to valid skill index's
                    case ConsoleKey.NumPad0:
                        skillIndex = 0;
                        break;
                    case ConsoleKey.NumPad1:
                        skillIndex = 1;
                        break;
                    case ConsoleKey.NumPad2:
                        skillIndex = 2;
                        break;
                    case ConsoleKey.NumPad3:
                        skillIndex = 3;
                        break;
                    default:
                        Console.WriteLine("Invalid input; try again");
                        continue;
                }

                // Next, try to find a target to cast it on
                Skill skill = skills.GetSkill(skillIndex);
                List<Character> charactersInRange = map.GetCharacterInRange(c, skill.GetRange());

                // If there are no characters in range, then reset this process
                if (charactersInRange.Count == 0)
                {
                    skillIndex = -1;
                    Console.WriteLine("No characters in range for this skill; pick again.");
                    continue;
                }

                // Otherwise, ask for input on which character to target
                while (target == null)
                {
                    int targetIndex = -1;
                    Console.WriteLine("Targets \t: ");
                    for (int i = 0; i < charactersInRange.Count; ++i)
                    {
                        Console.WriteLine("\t [{0}]: {1}", i, charactersInRange[i].GetName());
                    }
                    Console.WriteLine("\t [C]: Cancel Target Choice");

                    ConsoleKeyInfo targetInput = Console.ReadKey(true);
                    switch (targetInput.Key)
                    {
                        // TODO: Ensure inputs map to valid skill index's
                        // TODO: Support N characters and inputs; for now 4 is good enough
                        case ConsoleKey.NumPad0:
                            targetIndex = 0;
                            break;
                        case ConsoleKey.NumPad1:
                            targetIndex = 1;
                            break;
                        case ConsoleKey.NumPad2:
                            targetIndex = 2;
                            break;
                        case ConsoleKey.NumPad3:
                            targetIndex = 3;
                            break;
                        case ConsoleKey.C:
                            // TODO: Simplify this logic
                            skillIndex = -1;
                            break;
                        default:
                            Console.WriteLine("Invalid input; try again");
                            continue;
                    }

                    // We denote resetting the skill index to -1 as the "cancel" input
                    if (skillIndex == -1)
                    {
                        break;
                    }

                    if (targetIndex > charactersInRange.Count)
                    {
                        Console.WriteLine("Invalid input; try again");
                        continue;
                    }

                    target = charactersInRange[targetIndex];
                }
            }

            // If we get here, then we have a valid skill and target. Return them
            InputSkillPayload payload = new InputSkillPayload();
            payload.skillIndex = skillIndex;
            payload.target = target;
            return payload;
        }
    }
}
