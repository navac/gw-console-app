﻿using System.Collections;
using System.Collections.Generic;

class FireDealtIncrease : Buff
{
    public FireDealtIncrease(Character source, Skill sourceSkill) : base(source, sourceSkill)
    {
        // For 1 turn, increases all fire damage by 50%
    }

    public override void OnApply()
    {
        m_turns = 1;
    }

    public override void OnCasterSkillExecuting(Skill skill, Character caster, Character target)
    {
        DamagePayload damagePayload = skill.GetDamagePayload();
        if (damagePayload != null && damagePayload.GetDamageType() == DamagePayload.Type.Fire)
        {
            damagePayload.Multiply(0.5f);
        }
    }
}
