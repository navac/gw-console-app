﻿public class Buff
{
    protected int m_turns;
    protected Character m_source;
    protected Skill m_sourceSkill;

    public Buff(Character source, Skill sourceSkill)
    {
        m_source = source;
        m_sourceSkill = sourceSkill;
    }

    public virtual void OnApply() { }
    public virtual void OnRemove() { }

    public virtual void OnCasterSkillExecuting(Skill skill, Character caster, Character target) { }
    public virtual void OnTargetSkillExecuting(Skill skill, Character caster, Character target) { }
    public virtual void OnCasterSkillExecuted(Skill skill, Character caster, Character target) { }
    public virtual void OnTargetSkillExecuted(Skill skill, Character caster, Character target) { }

    public virtual void OnTurn() { m_turns--;  }

    public virtual bool IsExpired() { return m_turns < 0; }

    public Skill GetSourceSkill() { return m_sourceSkill; }
}
