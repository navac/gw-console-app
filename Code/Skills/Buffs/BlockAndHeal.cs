﻿using System.Collections;
using System.Collections.Generic;

class BlockAndHeal : Buff
{
    private bool m_used = false;

    public BlockAndHeal(Character source, Skill sourceSkill) : base(source, sourceSkill)
    {
        // For 3 turns or on next damage, block that damage and heal for that much instead
    }

    public override void OnApply()
    {
        m_turns = 3;
    }

    public override bool IsExpired()
    {
        return m_used || base.IsExpired();
    }

    public override void OnTargetSkillExecuting(Skill skill, Character caster, Character target)
    {
        DamagePayload damagePayload = skill.GetDamagePayload();
        if (damagePayload == null)
        {
            return;
        }

        int damage = damagePayload.GetDamage();
        if (damage > 0 && !m_used)
        {
            System.Console.WriteLine("{0} damage blocked and healed", damage);
            damagePayload.Clear();
            target.Heal(damage);
            m_used = true;
        }
    }
}
