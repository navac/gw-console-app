﻿using System;
using System.Collections;
using System.Collections.Generic;

class LightningJavelin : Skill
{
    public LightningJavelin() : base("Lightning Javelin", 5, 3)
    {
    }

    public override bool CanCast(Character caster, Character target)
    {
        return base.CanCast(caster, target) && target != null;
    }

    public override void PreExecute(Character caster, Character target)
    {
        m_damagePayload = new DamagePayload(DamagePayload.Type.Electric, 10);
    }

    public override void Execute(Character caster, Character target)
    {
        int damage = GetDamagePayload().GetDamage();
        Console.WriteLine("Dealing {0} damage", damage);
        target.TakeDamage(damage);
    }
}
