﻿using System;
using System.Collections;
using System.Collections.Generic;

public class DamagePayload
{
    public enum Type
    {
        Normal,
        Fire,
        Electric,
    }

    private enum ModifierType
    {
        Addition,
        Multiply,
        Clear,
    }

    private bool m_dirty;
    private int m_damage;
    private int m_base;
    private Type m_damageType;
    private Dictionary<ModifierType, List<float>> m_modifiers;

    public DamagePayload(Type type, int amount)
    {
        m_base = amount;
        m_damageType = type;
        m_modifiers = new Dictionary<ModifierType, List<float>>();
        m_modifiers.Add(ModifierType.Addition, new List<float>());
        m_modifiers.Add(ModifierType.Multiply, new List<float>());
        m_modifiers.Add(ModifierType.Clear, new List<float>());
        m_dirty = true;
    }

    private void Calculate()
    {
        // Create the base value by applying the addition mods
        int amount = m_base;
        foreach (float addition in m_modifiers[ModifierType.Addition])
        {
            amount += (int)Math.Round(addition);
        }

        // Use this amount as the base for the multiplier mods
        int multBase = amount;
        foreach (float percent in m_modifiers[ModifierType.Multiply])
        {
            amount += (int)Math.Round(multBase * percent);
        }

        // If there is a clear, we must abide by that 
        if (m_modifiers[ModifierType.Clear].Count > 0)
        {
            amount = 0;
        }

        m_damage = amount;
    }

    public int GetDamage()
    {
        if (m_dirty)
        {
            m_dirty = false;
            Calculate();
        }

        return m_damage;
    }

    public void Reset()
    {
        m_modifiers[ModifierType.Addition].Clear();
        m_modifiers[ModifierType.Multiply].Clear();
        m_modifiers[ModifierType.Clear].Clear();
    }

    public void Add(int amount)
    {
        m_dirty = true;
        m_modifiers[ModifierType.Multiply].Add(amount);
    }

    public void Multiply(float percentage)
    {
        m_dirty = true;
        m_modifiers[ModifierType.Multiply].Add(percentage);
    }

    public void Clear()
    {
        m_dirty = true;
        m_modifiers[ModifierType.Clear].Add(1);
    }

    public Type GetDamageType()
    {
        return m_damageType;
    }
}

