﻿using System;
using System.Collections;
using System.Collections.Generic;

class Fireball : Skill
{
    public Fireball() : base("Fireball", 5, 2)
    {
    }

    public override bool CanCast(Character caster, Character target)
    {
        return base.CanCast(caster, target) && target != null;
    }

    public override void PreExecute(Character caster, Character target)
    {
        m_damagePayload = new DamagePayload(DamagePayload.Type.Fire, 10);
    }

    public override void Execute(Character caster, Character target)
    {
        int damage = GetDamagePayload().GetDamage();
        Console.WriteLine("Dealing {0} damage", damage);
        target.TakeDamage(damage);
    }
}
