﻿using System.Collections;
using System.Collections.Generic;

class FireAttunement : Skill
{
    public FireAttunement() : base("Fire Attunement", 2, 0)
    {
    }

    public override bool CanCast(Character caster, Character target)
    {
        // Do not allow multiple FireIncrease buffs from FireAttunemnent skill
        foreach (Buff buff in target.GetBuffs())
        {
            if (buff is FireDealtIncrease && buff.GetSourceSkill() is FireAttunement)
            {
                return false;
            }
        }

        return base.CanCast(caster, target);
    }

    public override void Execute(Character caster, Character target)
    {
        caster.AddBuff(new FireDealtIncrease(caster, this));
    }
}
