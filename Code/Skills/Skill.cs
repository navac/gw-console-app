﻿public class Skill
{
    public Skill(string name, int cost, int range)
    {
        m_name = name;
        m_cost = cost;
        m_range = range;
    }

    protected string m_name;
    protected int m_cost;
    protected int m_range;
    protected DamagePayload m_damagePayload;

    public string GetName() { return m_name; }
    public int GetCost() { return m_cost; }
    public int GetRange() { return m_range; }

    public DamagePayload GetDamagePayload() { return m_damagePayload; }

    public virtual bool CanCast(Character caster, Character target) { return caster.GetEnergy() >= m_cost; }

    public virtual void PreExecute(Character caster, Character target) { }
    public virtual void Execute(Character caster, Character target) { }
    public virtual void PostExecute(Character caster, Character target) { }

    public virtual void OnOtherExecuting(Skill skill, Character caster, Character target) { }
    public virtual void OnOtherExecuted(Skill skill, Character caster, Character target) { }
}
