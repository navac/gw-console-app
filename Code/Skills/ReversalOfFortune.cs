﻿using System.Collections;
using System.Collections.Generic;

class ReversalOfFortune : Skill
{
    public ReversalOfFortune() : base("Reversal Of Fortune", 3, 2)
    {
    }

    public override bool CanCast(Character caster, Character target)
    {
        // Do not allow multiple BlockAndHeal buffs from ReversalOfFortune skill
        foreach (Buff buff in target.GetBuffs())
        {
            if (buff is BlockAndHeal && buff.GetSourceSkill() is ReversalOfFortune)
            {
                return false;
            }
        }

        return base.CanCast(caster, target);
    }

    public override void Execute(Character caster, Character target)
    {
        target.AddBuff(new BlockAndHeal(caster, this));
    }
}
