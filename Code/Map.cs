﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Map
{
    private int m_rows;
    private int m_columns;
    private Character[,] m_grid;

    // TODO: Make "locations"  a real struct, ie Vector2

    public Map(int row, int col)
    {
        m_rows = row;
        m_columns = col;
        m_grid = new Character[m_rows, m_columns];
    }

    public enum Direction
    {
        Right,
        Left,
        Up,
        Down,
    }

    public bool IsPositionTaken(int x, int y)
    {
        return m_grid[x, y] != null;
    }

    public void ClearPosition(int x, int y)
    {
        m_grid[x, y] = null;
    }

    public void ClearCharacter(Character c)
    {
        int[] location = GetLocation(c);
        if (location != null)
        {
            ClearPosition(location[0], location[1]);
        }
    }

    public bool PlaceCharacter(Character c, int x, int y)
    {
        // Cannot place something that is take up space
        if (!IsValidLocation(x, y) || IsPositionTaken(x, y))
        {
            return false;
        }

        m_grid[x, y] = c;
        return true;
    }

    public bool MoveCharacter(Character c, Direction d)
    {
        int[] old = GetLocation(c);
        int[] loc = GetProposedLocation(c, d);
        if (!IsValidLocation(loc[0], loc[1]) || IsPositionTaken(loc[0], loc[1]))
        {
            return false;
        }

        ClearPosition(old[0], old[1]);
        PlaceCharacter(c, loc[0], loc[1]);

        return true;
    }

    public int[] GetProposedLocation(Character c, Direction d)
    {
        int[] loc = GetLocation(c);
        switch (d)
        {
            case Direction.Right:
                loc[1]++;
                break;
            case Direction.Left:
                loc[1]--;
                break;
            case Direction.Up:
                loc[0]--;
                break;
            case Direction.Down:
                loc[0]++;
                break;
        }

        return loc;
    }

    public bool IsValidLocation(int row, int col)
    {
        // Cannot place something outside the bounds of the map
        if (row < 0 || row >= m_rows || col < 0 || col >= m_columns)
        {
            return false;
        }

        // Cannot place something that is take up space
        if (IsPositionTaken(row, col))
        {
            return false;
        }

        return true;
    }

    private int[] GetLocation(Character c)
    {
        for (int i = 0; i < m_rows; ++i)
        {
            for (int j = 0; j < m_columns; ++j)
            {
                if (m_grid[i, j] == c)
                {
                    return new int[] { i, j };
                }
            }
        }

        return null;
    }

    //--------------------------------------------------------
    public void Print()
    {
        Console.WriteLine();

        for (int i = 0; i < m_rows; ++i)
        {
            string row = "|";
            for (int j = 0; j < m_columns; ++j)
            {
                Character c = m_grid[i, j];
                row += c == null ? "-" : c.GetName();
                row += "|";
            }
            Console.WriteLine(row);
        }

        Console.WriteLine();
    }

    public List<Character> GetCharacterInRange(Character c, int range)
    {
        List<Character> characters = new List<Character>();
        int[] location = GetLocation(c);
        int startRow = Math.Max(0, location[0] - range);
        int endRow = Math.Min(m_rows - 1, location[0] + range);
        int startCol = Math.Max(0, location[1] - range);
        int endCol = Math.Min(m_columns - 1, location[1] + range);
        for (int row = startRow; row <= endRow; ++row)
        {
            for (int col = startCol; col <= endCol; ++col)
            {
                if (m_grid[row, col] != null)
                {
                    characters.Add(m_grid[row, col]);
                }
            }
        }

        return characters;
    }
}
