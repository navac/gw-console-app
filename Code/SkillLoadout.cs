﻿using System;
using System.Collections;
using System.Collections.Generic;

public class SkillLoadout
{
    private List<Skill> m_skills = new List<Skill>();
    private Character m_owner;

    public SkillLoadout(Character owner)
    {
        m_owner = owner;
    }

    public void AddSkill(Skill skill)
    {
        m_skills.Add(skill);
    }

    public Skill GetSkill(int index)
    {
        if (index < 0 || index >= m_skills.Count)
        {
            return null;
        }

        return m_skills[index];
    }

    public void Cast(int index, Character target)
    {
        // Make sure we are castable
        Skill skill = m_skills[index];
        if (!skill.CanCast(m_owner, target))
        {
            return;
        }

        Console.WriteLine(m_owner.GetName() + " casting " + skill.GetName() + " on " + target.GetName());

        // Inform all other skills in the loadout that this skill is casting
        skill.PreExecute(m_owner, target);
        for (int i = 0; i < m_skills.Count; i++)
        {
            Skill itrSkill = m_skills[i];
            if (itrSkill == skill) continue;

            itrSkill.OnOtherExecuting(skill, m_owner, target);
        }

        // Inform all buffs on caster and target that this skill is executing
        List<Buff> casterBuffs = m_owner.GetBuffs();
        List<Buff> targetBuffs = target.GetBuffs();
        casterBuffs.ForEach((Buff buff) =>
        {
            buff.OnCasterSkillExecuting(skill, m_owner, target);
        });
        targetBuffs.ForEach((Buff buff) =>
        {
            buff.OnTargetSkillExecuting(skill, m_owner, target);
        });

        // Cast the skill
        skill.Execute(m_owner, target);

        // Inform all the other skills in the loadout that this skill has casted
        skill.PostExecute(m_owner, target);
        for (int i = 0; i < m_skills.Count; i++)
        {
            Skill itrSkill = m_skills[i];
            if (itrSkill == skill) continue;

            itrSkill.OnOtherExecuted(skill, m_owner, target);
        }

        // Inform all buffs on caster and target that this skill has executed
        casterBuffs.ForEach((Buff buff) =>
        {
            buff.OnCasterSkillExecuted(skill, m_owner, target);
        });
        targetBuffs.ForEach((Buff buff) =>
        {
            buff.OnTargetSkillExecuted(skill, m_owner, target);
        });

        // Clear the skill's damage payload
        DamagePayload payload = skill.GetDamagePayload();
        if (payload != null)
        {
            payload.Reset();
        }
    }

    public void Print()
    {
        Console.WriteLine("Skills \t: ");
        for (int i = 0; i < m_skills.Count; ++i)
        {
            Console.WriteLine("\t [{0}]: {1}", i, m_skills[i].GetName()); 
        }
    }
}
