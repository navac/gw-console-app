﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Character
{
    private string m_name;
    private int m_energy;
    private int m_team;
    private int m_health;

    private List<Buff> m_buffs;
    private SkillLoadout m_skills;

    public Character(
        string name,
        int health,
        int energy,
        int team)
    {
        m_buffs = new List<Buff>();
        m_skills = new SkillLoadout(this);

        m_name = name;
        m_energy = energy;
        m_health = health;
        m_team = team;
    }

    public string GetName()
    {
        return m_name;
    }

    public int GetEnergy()
    {
        return m_energy;
    }

    public bool IsDead()
    {
        return m_health <= 0;
    }

    public List<Buff> GetBuffs()
    {
        return m_buffs;
    }

    public void AddBuff(Buff buff)
    {
        Console.WriteLine("Buff " + buff.GetType().ToString() + " added");
        m_buffs.Add(buff);
        buff.OnApply();
    }

    public void RemoveBuff(Buff buff)
    {
        Console.WriteLine("Buff " + buff.GetType().ToString() + " removed from ");
        m_buffs.Remove(buff);
        buff.OnRemove();
    }

    public SkillLoadout GetSkills()
    {
        return m_skills;
    }

    public void TakeDamage(int amount)
    {
        m_health -= amount;
    }

    public void Heal(int amount)
    {
        m_health += amount;
    }

    public int GetHealth()
    {
        return m_health;
    }

    public void OnTurn()
    {
        // Tick all buffs every turn
        foreach (Buff buff in m_buffs)
        {
            buff.OnTurn();

            // If we're going to remove this buff this turn,
            // inform it of this action
            if (buff.IsExpired())
            {
                Console.WriteLine("{0} has expired from {1}", buff.GetType().ToString(), m_name);
                buff.OnRemove();
            }
        }

        // Clear expired buffs
        m_buffs = m_buffs.FindAll((Buff b) =>
        {
            return !b.IsExpired();
        });
    }

    public void OnDeath()
    {
        Console.WriteLine("{0} has died!", m_name);
    }

    public void Print()
    {
        Console.WriteLine(m_name + " \t:");
        Console.WriteLine("HP \t: " + GetHealth());
    }
}
